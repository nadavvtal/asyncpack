export async function some(
  iterable: Iterable<any> | Promise<Iterable<any>>,
  num: number
): Promise<any> {
  iterable = await iterable;
  iterable = Array.from(iterable);

  const results: any[] = [];
  return await new Promise((resolve) => {
    (iterable as Array<any>).forEach(async (p) => {
      results.push(await p);
      if (results.length === num) resolve(results);
    });
  });
}
