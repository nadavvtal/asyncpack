import log from "@ajar/marker";

export async function each(
  promiseArr: Iterable<any> | Promise<Iterable<any>>,
  iterableFunciton: (a: any) => any | Promise<any>
): Promise<any[]> {
  try {
    log.green("Start running");
    promiseArr = await promiseArr;
    promiseArr = Array.from(promiseArr);

    for (let i = 0; i < (promiseArr as Array<any>).length; i++) {
      (promiseArr as Array<any>)[i] = await (promiseArr as Array<any>)[i];
      (promiseArr as Array<any>)[i] = await iterableFunciton(
        (promiseArr as Array<any>)[i]
      );
    }
    return promiseArr as Array<any>;
  } catch (err) {
    console.log(err);
    throw new Error(err as string);
  }
}
