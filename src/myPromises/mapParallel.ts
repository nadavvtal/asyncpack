import log from "@ajar/marker";
import { promisify } from "util";
export async function mapP(
  promiseArr: Iterable<any> | Promise<Iterable<any>>,
  iterableFunciton: (a: any) => any | Promise<any>
) {
  try {
    log.green("Start running");
    promiseArr = await promiseArr;
    promiseArr = Array.from(promiseArr);

    const iterateArr = (promiseArr as Array<any>).map(async function (promise) {
      await promise;
      return iterableFunciton(promise);
    });
    const resolvedArr = await Promise.all(iterateArr);
    return resolvedArr;
  } catch (err) {
    log.red("Error", err);
  }
}
