export async function reduce(
  iterable: Iterable<any> | Promise<Iterable<any>>,
  cb: (ac: any, cur: any) => any | Promise<any>,
  initial: any
): Promise<any> {
  iterable = await iterable;
  iterable = Array.from(iterable);

  let finalAccumulator = initial || (iterable as Array<any>)[0];

  for (const item of iterable) {
    await item;
    finalAccumulator = await cb(finalAccumulator, item);
  }

  return finalAccumulator;
}
