export async function race(
  promises: Iterable<any> | Promise<Iterable<any>>
): Promise<any> {
  promises = await promises;
  promises = Array.from(promises);

  return await new Promise((resolve) => {
    (promises as Array<Promise<any>>).forEach(async (promise) => {
      resolve(await promise);
    });
  });
}
