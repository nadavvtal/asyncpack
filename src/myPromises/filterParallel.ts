export async function filterP(
  promiseArr: Iterable<any> | Promise<Iterable<any>>,
  booleanFunction: (a: any) => boolean | Promise<boolean>
): Promise<any[]> {
  try {
    console.log("Start running");
    promiseArr = await promiseArr;
    promiseArr = Array.from(promiseArr);
    const res = [];
    const pendingArr = (promiseArr as Array<any>).map(async (item) => {
      return booleanFunction(await item);
    });
    for (const [i, p] of pendingArr.entries()) {
      if (await p) res.push((promiseArr as Array<any>)[i]);
    }
    return res;
  } catch (err) {
    console.log("error");
    throw new Error(err as string);
  }
}
