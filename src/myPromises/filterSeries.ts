export async function filterS(
  promiseArr: Iterable<any> | Promise<Iterable<any>>,
  booleanFunction: (a: any) => boolean | Promise<boolean>
): Promise<any[]> {
  try {
    console.log("Start running");
    promiseArr = await promiseArr;
    const resultArr = [];

    for (const promise of promiseArr) {
      const resolved = await promise;
      if ((await booleanFunction(resolved)) === true) {
        resultArr.push(resolved);
      }
    }
    return resultArr;
  } catch (err) {
    console.log(err);
    throw new Error(err as string);
  }
}
