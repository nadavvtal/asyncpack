export async function all(
  arrayPromises: Iterable<any> | Promise<Iterable<any>>
): Promise<Array<any>> {
  try {
    arrayPromises = await arrayPromises;
    arrayPromises = Array.from(arrayPromises);

    const resolvedArr: any[] = [];
    for (const promise of arrayPromises) {
      resolvedArr.push(await promise);
    }
    return resolvedArr;
  } catch (err) {
    console.log("err");
    throw new Error(err as string);
  }
}
