import log from "@ajar/marker";

interface myObject {
  [k: string]: any;
}

export async function props(
  promiseObj: myObject | Map<any, any> | Promise<myObject | Map<any, any>>
): Promise<myObject | Map<any, any>> {
  try {
    log.green("Start running");
    promiseObj = await promiseObj;
    const res: myObject = {};
    for (const promise in promiseObj) {
      res[promise] = await (promiseObj as myObject)[promise];
    }
    return res;
  } catch (err) {
    log.red("Error", err);
    throw new Error(err as string);
  }
}
