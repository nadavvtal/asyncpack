import log from "@ajar/marker";

export async function mapS(
  promiseArr: Iterable<any> | Promise<Iterable<any>>,
  itrFunction: (a: any) => any | Promise<any>
) {
  try {
    log.green("Start running");
    promiseArr = await promiseArr;
    promiseArr = Array.from(promiseArr);

    const resultsArr = [];
    for (let i = 0; i < (promiseArr as Array<any>).length; i++) {
      const resolvedAndmaped = await itrFunction(
        await (promiseArr as Array<any>)[i]
      );
      resultsArr.push(resolvedAndmaped);
    }
    return resultsArr;
  } catch (err) {
    log.red("Error", err);
    throw new Error(err as string);
  }
}
