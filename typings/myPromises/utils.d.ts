export declare const delay: (ms: number) => Promise<any>;
export declare const echo: (msg: string, ms: number) => Promise<any>;
export declare const random: (max: number, min?: number) => number;
