export declare function all(arrayPromises: Iterable<any> | Promise<Iterable<any>>): Promise<Array<any>>;
