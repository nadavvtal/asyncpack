export declare function filterP(promiseArr: Iterable<any> | Promise<Iterable<any>>, booleanFunction: (a: any) => boolean | Promise<boolean>): Promise<any[]>;
