export declare function each(promiseArr: Iterable<any> | Promise<Iterable<any>>, iterableFunciton: (a: any) => any | Promise<any>): Promise<any[]>;
